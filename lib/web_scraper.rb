module WebScraper
  require 'nokogiri'
  require 'open-uri'  

  BASE_URL = "https://apkpure.com"
  
  class Scraper

    class << self

      def fetch_categories link
        doc        = get_full_body link
        categories = fetch_categories_url doc
        categories.each do |category|
          Category.create(name: category.text, url: BASE_URL+category.attributes["href"].value)
        end 
      end 

      def get_categorized_apps
        Category.all.each do |category|
          doc_category  =  get_full_body category.url
          total_pages   =  get_max_page_in_category doc_category
          (1..total_pages.to_i).to_a.each do |page_number|
            doc_page_apps = get_full_body category.url+"?page=#{page_number}"
            get_apps_from_url doc_page_apps, category
          end
        end  
      end

      def get_apps_from_url doc, category
        doc.css('.category-template li .category-template-img').each do |x|
        app_doc       = get_full_body BASE_URL+x.children.first.attributes["href"].value
        App.create(name: get_app_name(app_doc), play_store_url: get_play_store_link(app_doc))
        end
      end   


      private
        def get_full_body link
          Nokogiri::HTML(open(link)) 
        end 

        def fetch_categories_url doc
          doc.css('.index-category li a')
        end 

        def get_app_link doc
          BASE_URL+doc.css('.category-template-img').first.children.first.attributes["href"].value
        end

        def get_app_name doc
          doc.css('.title-like h1').children.text
        end 

        def get_play_store_link doc
          doc.css('li p .ga').first.attributes["href"].value
        end  

        def get_max_page_in_category doc
          doc.css('.loadmore').first.attributes["data-maxpage"].value
        end 
    end

  end
end