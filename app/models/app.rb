class App
  include Mongoid::Document
  field :name, type: String
  field :play_store_url, type: String
end
